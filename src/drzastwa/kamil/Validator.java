package drzastwa.kamil;

import java.util.function.Function;

class ValidationException extends Exception{
    String msg;
    public ValidationException(String msg){
        this.msg = msg;
    }
    @Override
    public String toString() {
        return "Validation error: " + msg;
    }

    @Override
    public String getMessage() {
        return this.msg;
    }
}

public class Validator {
    private String[] validationErrors;
    private String regex;
    private Function<String, Boolean> customValidateFunction = e -> true;

    public Validator(String regex, String[] validationErrors) {
        this.regex = regex;
        this.validationErrors = validationErrors;
    }

    public Validator(String regex, Function<String, Boolean> customValidateFunction,String[] validationErrors) {
        this.regex = regex;
        this.customValidateFunction = customValidateFunction;
        this.validationErrors = validationErrors;
    }

    public void validate(String input) throws Exception {
        if (!input.matches(regex)) {
            throw new ValidationException(validationErrors[0]);
        }
        if (!customValidateFunction.apply(input)) {
            throw new ValidationException(validationErrors[1]);
        }
    }
}
