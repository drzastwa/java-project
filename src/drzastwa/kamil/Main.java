package drzastwa.kamil;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        final Function<String, Boolean> validatePeselControlSum = new Function<String, Boolean>() {
            @Override
            public Boolean apply(String pesel) {
                ArrayList<Integer> weights = new ArrayList<Integer>(List.of(
                        9, 7, 3, 1, 9, 7, 3, 1, 9, 7
                ));

                int sum = 0;
                for (char character : pesel.substring(0, pesel.length() - 1).toCharArray()) {
                    sum += weights.remove(0) * Character.getNumericValue(character);
                }
                int controlSum = sum % 10;
                return pesel.endsWith(Integer.toString(controlSum));
            }
        };

        final String emptyLineRegex = "^(?!\\s*$).+";
        final Validator emptyLineValidator = new Validator(emptyLineRegex, new String[]{"puste linie nie są dozwolone"});
        final String peselRegex = "^\\d{11}$";
        final Validator peselValidator = new Validator(
                peselRegex,
                validatePeselControlSum,
                new String[]{"pesel musi składać się z 11 cyfr", "suma kontrolna peselu się nie zgadza"}
                );



        final InputObject[] inputObjects = new InputObject[]{
                new InputObject("Podaj Imie: ", emptyLineValidator),
                new InputObject("Podaj Nazwisko: ",  emptyLineValidator),
                new InputObject("Podaj Miasto: ", emptyLineValidator),
                new InputObject("Podaj Pesel: ", peselValidator)
        };

        final Reader reader = new Reader(inputObjects);
        final UserFile userFile = new UserFile("uzytkownicy");

        while (true) {
            String newData = reader.read();
            int action = userFile.newUser(newData);
            if (action  == 0){
                System.out.println("Wykryto duplikat. Użytkownik znajduje się już w bazie");
            } else if (action > 0){
                System.out.println("Ten pesel występował już w bazie. Użytkownik został zaktualizowany");
            } else {
                System.out.println("Użytkownik dodany");
            }

        }
    }

}
