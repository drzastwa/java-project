package drzastwa.kamil;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

public class UserFile {
    FileWriter fileWriter;
    File file;

    public UserFile() {
        String uniqueID = UUID.randomUUID().toString();
        String path = uniqueID + ".txt";
        createFile(path);
    }

    public UserFile(String filename){
        String path = filename + ".txt";
        createFile(path);
    }

    private void createFile(String path){
        try{
            file = new File(path);
            file.createNewFile();
            fileWriter = new FileWriter(file);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int newUser(String data){
        int lineIndex = 0;
        try {
            lineIndex = containsUser(data);
            if (lineIndex > 0){
                replaceUser(lineIndex, data);
            }
            else if (lineIndex == -1){
                fileWriter.write(data);
                fileWriter.write(System.getProperty( "line.separator" ));
                fileWriter.flush();
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return lineIndex;
    }

    public int containsUser(String data){
        String[] userData = data.split(";");
        String name = userData[0];
        String surname = userData[1];
        String city = userData[2];
        String pesel = userData[3];
        try {
            Scanner fileScanner = new Scanner(file);
            int lineIndex = 0;
            while (fileScanner.hasNextLine()) {
                String line = fileScanner.nextLine();
                String[] lineData = line.split(";");
                String lineName = lineData[0];
                String lineSurname = lineData[1];
                String lineCity = lineData[2];
                String linePesel = lineData[3];
                lineIndex++;

                if (linePesel.equals(pesel) && !(lineName.equals(name) && lineSurname.equals(surname) && lineCity.equals(city))) {
                    return lineIndex;
                } else if(linePesel.equals(pesel)){
                    return 0;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    public void replaceUser(int lineIndex,String data) throws IOException {
        Path path = Paths.get(file.getPath());
        List<String> fileLines = Files.readAllLines(path, StandardCharsets.UTF_8);
        fileLines.set(lineIndex - 1, data);
        Files.write(path, fileLines, StandardCharsets.UTF_8);
    }

    public long getFileSize(){
        return file.length();
    }

}
