package drzastwa.kamil;

public class Reader {
    private InputObject[] inputObjects;

    public Reader(InputObject[] inputObjects) {
        this.inputObjects = inputObjects;
    }

    public String read() {
        String result = "";
        for (InputObject inputObject : inputObjects) {
            String input= "";
            while (true){
                try {
                    input = inputObject.getInput();
                    break;
                } catch (Exception e){
                }
            }
            result += input + ";";
        }
        return result;
    }

}
