package drzastwa.kamil;

import java.util.Scanner;

public class InputObject {
    private String message = "";
    private Validator validator;

    public InputObject(String message, Validator validator) {
        this.message = message;
        this.validator = validator;
    }

    public String getInput() throws Exception {
        System.out.print(message);
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine().trim();

        try {
            validator.validate(input);
        } catch (Exception e) {
            System.out.println(e.toString());
            throw new Exception("reading error");
        }
        return input;
    }


}
