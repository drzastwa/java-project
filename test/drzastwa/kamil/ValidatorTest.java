package drzastwa.kamil;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.function.Function;

public class ValidatorTest {
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void regexNotMatched_ExceptionThrown() throws Exception{
        final String errorMessage = "input should start with number";
        Validator validator = new Validator("^\\d", new String[]{errorMessage});
        exceptionRule.expect(ValidationException.class);
        exceptionRule.expectMessage(errorMessage);
        validator.validate("abc");
    }

    @Test
    public void regexMatched_NoExceptionThrown() throws Exception{
        final String errorMessage = "input should have just one digit";
        Validator validator = new Validator("^\\d", new String[]{errorMessage});
        validator.validate("1");
    }

    @Test
    public void customValidationNotPassed_ExceptionThrown() throws Exception{
        final String errorMessage = "input should have just one digit";
        final String customFunctionErrorMsg = "length of string should be 3";
        final Function<String, Boolean> validateLength = new Function<String, Boolean>() {
            @Override
            public Boolean apply(String s) {
                return s.length() == 3;
            }
        };

        Validator validator = new Validator("^\\d", validateLength, new String[]{errorMessage, customFunctionErrorMsg});
        exceptionRule.expect(ValidationException.class);
        exceptionRule.expectMessage(customFunctionErrorMsg);
        validator.validate("1");
    }


    @Test
    public void customValidationPassed_NoExceptionThrown() throws Exception{
        final String errorMessage = "input should have just one digit";
        final String customFuctionErorrMessage = "length of string should be 3";
        final Function<String, Boolean> validateLength = new Function<String, Boolean>() {
            @Override
            public Boolean apply(String s) {
                return s.length() == 3;
            }
        };

        Validator validator = new Validator("^\\d..", validateLength, new String[]{errorMessage, customFuctionErorrMessage});
        validator.validate("123");
    }
}