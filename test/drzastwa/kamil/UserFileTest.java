package drzastwa.kamil;

import org.junit.Test;

import static org.junit.Assert.*;

public class UserFileTest {

    final String fileName = "testowy";
    UserFile userFile = new UserFile(fileName);

    @Test
    public void addingNewUserTest() {
        final long initialFileSize = userFile.getFileSize();
        final String newUser = "Jan;Nowak;szczecin;12345";
        assert(userFile.containsUser(newUser) == -1);
        userFile.newUser(newUser);
        assert(userFile.containsUser(newUser) == 0);
        final long currentFileSize = userFile.getFileSize();
        assertTrue(initialFileSize < currentFileSize);
    }

    @Test
    public void replacingExistingUserTest() {
        final String newUser = "Jan;Nowak;szczecin;12345";
        userFile.newUser(newUser);
        final long initialFileSize = userFile.getFileSize();

        //switching second parameter(surname) from Nowak to Kowal - same file length
        final String updatedUser = "Jan;Kowal;szczecin;12345";
        assert(userFile.containsUser(updatedUser) > 0);
        userFile.newUser(updatedUser);
        assert(userFile.containsUser(updatedUser) == 0);
        final long currentFileSize = userFile.getFileSize();
        assert(initialFileSize == currentFileSize);
    }

    @Test
    public void addingSameUserTwiceTest(){
        final String newUser = "Jan;Nowak;szczecin;12345";
        assert(userFile.containsUser(newUser) == -1);
        userFile.newUser(newUser);
        assert(userFile.containsUser(newUser) == 0);
        final long initialFileSize = userFile.getFileSize();

        userFile.newUser(newUser);
        assert(userFile.containsUser(newUser) == 0);


        final long currentFileSize = userFile.getFileSize();
        assert(initialFileSize == currentFileSize);

    }

}